import React, { useEffect, useState } from "react";
import { getColumn } from "../services";

const ColumnPage = () => {
  const [column, setColumn] = useState([]);

  const [offset, setOffset] = useState(0);

  const limit = 8;


  const loadMore = () => {
    setOffset(offset+limit)
  }

  useEffect(() => {
    const data = getColumn(limit, offset);
    setColumn([...column, ...data]);
  }, [offset]);

  return (
    <div className="container" style={{ marginTop: 56, marginBottom: 53 }}>
      <div className="d-flex justify-content-between">
        <div className="recommended">
          <p className="text-label">
            RECOMMENDED <br /> COLUMN
          </p>

          <div className="w-25 mx-auto">
            <div className="hr" />
          </div>
          <p className="text-white text18">オススメ</p>
        </div>

        <div className="recommended">
          <p className="text-label">
            RECOMMENDED <br /> DIET
          </p>
          <div className="w-25 mx-auto">
            <div className="hr" />
          </div>
          <p className="text-white text18">ダイエット</p>
        </div>

        <div className="recommended">
          <p className="text-label">
            RECOMMENDED <br /> BEAUTY
          </p>
          <div className="w-25 mx-auto">
            <div className="hr" />
          </div>
          <p className="text-white text18">美容</p>
        </div>

        <div className="recommended">
          <p className="text-label">
            RECOMMENDED <br /> HEALTH
          </p>
          <div className="w-25 mx-auto">
            <div className="hr" />
          </div>
          <p className="text-white text18">健康</p>
        </div>
      </div>

      <div style={{ marginTop: 56 }} className="d-flex justify-content-between flex-wrap">
        {column.map((i) => (
          <div key={i.id} className="card-colum">
            <div className="position-relative">
              <img
                alt="colum"
                className="w-100 h-100"
                src={require(`../assets/images/${i?.src}`)}
              />
              <div className="date">
                <p className="text-date">
                  2021.05.17 <span style={{ marginLeft: 10 }}>23:25</span>
                </p>
              </div>
            </div>
            <div className="dots">
              魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…
            </div>

            <p className="text-colum">
              #魚料理 <span className="mx-3">#和食 </span> #DHA
            </p>
          </div>
        ))}
      </div>

      <div className="text-center">
        <div className="btn-more" onClick={loadMore}>
          <p className="text-more">自分の日記をもっと見る</p>
        </div>
      </div>
    </div>
  );
};

export default ColumnPage;
