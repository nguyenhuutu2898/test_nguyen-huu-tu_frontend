import React, { useEffect, useState } from "react";
import { getTopPage } from "../services";

const TopPage = () => {
  const [data, setData] = useState([]);

  const [offset, setOffset] = useState(0);

  const limit = 8;

  const loadMore = () => {
    setOffset(offset + limit);
  };

  useEffect(() => {
    const res = getTopPage(limit, offset);
    setData([...data, ...res]);
  }, [offset]);
  return (
    <div>
      <div className="row mx-0">
        <div className="col-5 p-0" style={{ height: 316 }}>
          <img
            className="w-100 h-100"
            style={{ objectFit: "cover" }}
            alt="d01"
            src={require("../assets/images/d01.jpg")}
          />
        </div>
        <div
          className="col-7 p-0"
          style={{ backgroundColor: "#2E2E2E", height: 316 }}
        >
          <img
            className="w-100 h-100"
            style={{ objectFit: "contain" }}
            alt="main_graph"
            src={require("../assets/images/main_graph.png")}
          />
        </div>
      </div>

      <div className="container">
        <div className="d-flex justify-content-center gap-5 m-4">
          <div className="filter">
            <img
              alt="icon_knife"
              src={require("../assets/images/icon_knife.png")}
            />
            <p className="text-icon">Morning</p>
          </div>
          <div className="filter">
            <img
              alt="icon_knife"
              src={require("../assets/images/icon_knife.png")}
            />
            <p className="text-icon">Lunch</p>
          </div>
          <div className="filter">
            <img
              alt="icon_knife"
              src={require("../assets/images/icon_knife.png")}
            />
            <p className="text-icon">Dinner</p>
          </div>
          <div className="filter">
            <img
              alt="icon_knife"
              src={require("../assets/images/icon_cup.png")}
            />
            <p className="text-icon">Snack</p>
          </div>
        </div>
        <div>
          <div className="d-flex justify-content-between flex-wrap">
            {data.map((i) => (
              <div key={i.id} className="card-colum position-relative mt-3">
                <img
                  alt="colum"
                  className="w-100 h-100"
                  src={require(`../assets/images/${i?.src}`)}
                />
                <div className="date">
                  <p className="text-date">
                    2021.05.17 <span style={{ marginLeft: 10 }}>23:25</span>
                  </p>
                </div>
              </div>
            ))}
          </div>

          <div className="text-center mt-3">
            <div className="btn-more" onClick={loadMore}>
              <p className="text-more">自分の日記をもっと見る</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TopPage;
