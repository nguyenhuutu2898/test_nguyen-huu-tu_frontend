import React from "react";

const MyRecord = () => {
  return (
    <div className="container" style={{ marginTop: 56 }}>
      <div className="d-flex justify-content-between">
        <div className="records">
          <div className="card-records">
            <div
              style={{
                backgroundImage: `url(${require("../assets/images/MyRecommend-1.jpg")})`,
              }}
              className="img-records"
            ></div>
          </div>
          <div className="card-records" style={{background: 'none'}}>
            <p className="text-label-records">BODY RECORD</p>
            <div className="text-records">自分のカラダの記録</div>
          </div>
        </div>

        <div className="records">
          <div className="card-records">
            <div
              style={{
                backgroundImage: `url(${require("../assets/images/MyRecommend-2.jpg")})`,
              }}
              className="img-records"
            ></div>
          </div>
          <div className="card-records" style={{background: 'none'}}>
            <p className="text-label-records">MY EXERCISE</p>
            <div className="text-records">自分の運動の記録</div>
          </div>
        </div>

        <div className="records">
          <div className="card-records">
            <div
              style={{
                backgroundImage: `url(${require("../assets/images/MyRecommend-3.jpg")})`,
              }}
              className="img-records"
            ></div>
          </div>
          <div className="card-records" style={{background: 'none'}}>
            <p className="text-label-records">MY DIARY</p>
            <div className="text-records">自分の日記</div>
          </div>
        </div>
      </div>

      <div>
       <img alt="RECORD" className="w-100 mt-5" src={require('../assets/images/BODY RECORD.png')} />
      </div>
    </div>
  );
};

export default MyRecord;
