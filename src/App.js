import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import { Route, Routes, Navigate, Outlet } from "react-router-dom";

import "./assets/styles/main.css";
import ColumnPage from "./pages/ColumnPage";
import TopPage from "./pages/TopPage";
import MyRecord from "./pages/MyRecord";
import Header from "./components/Header";
import Footer from "./components/Footer";
import ScrollTop from "./components/ScrollTop";

const PrivateRoutes = () => {
  const isAuth = localStorage.getItem("isAuth");

  return JSON.parse(isAuth) ? <Outlet /> : <Navigate to="/login" />;
};

function App() {
  return (
    <div>
      <Header />
      <Routes>
        <Route path="/" element={<ColumnPage />} />
        <Route path="/column" element={<ColumnPage />} />
        <Route path="login" element={<ColumnPage />} />
        <Route path="*" element={<ColumnPage />} />

        <Route element={<PrivateRoutes />}>
          <Route path="/top" element={<TopPage />} />
          <Route path="/myRecord" element={<MyRecord />} />
        </Route>
      </Routes>

      <ScrollTop />

      <Footer/>

      
    </div>
  );
}

export default App;
