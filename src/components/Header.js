import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";

const Header = () => {
  const isAuth = localStorage.getItem("isAuth");
  const navigate = useNavigate();

  const [isMenu, setIsMenu] = useState(false);
  const [isLogin, setIsLogin] = useState(false);

  const onClick = () => {
    setIsMenu(!isMenu);
  };

  const onPress = () => {
    localStorage.setItem("isAuth", JSON.stringify(!JSON.parse(isAuth)));
    if (JSON.parse(isAuth)) {
      navigate("/column");
    } else {
      navigate("/top");
    }
  };

  useEffect(() => {
    setIsLogin(JSON.parse(isAuth));
  }, [isAuth]);

  return (
    <div className="bg-header">
      <div className="container header">
        <div className="logo">
          <img
            onClick={() => navigate("/column")}
            alt="logo"
            src={require("../assets/images/logo.png")}
          />
        </div>

        <div className="menu">
          <div className="d-flex align-items-center">
            <img
              className="icon-menu"
              alt="meno"
              src={require("../assets/images/icon_memo.png")}
            />
            <p className="text-menu">自分の記録</p>
          </div>
          <div className="d-flex align-items-center mx-4">
            <img
              className="icon-menu"
              alt="challenge"
              src={require("../assets/images/icon_challenge.png")}
            />
            <p className="text-menu">チャレンジ </p>
          </div>
          <div className="d-flex align-items-center">
            <img
              className="icon-menu"
              alt="info"
              src={require("../assets/images/icon_info.png")}
            />
            <p className="text-menu">お知らせ</p>
          </div>

          <div className="btn-menu" onClick={onClick}>
            <img
              className="icon-menu"
              alt="menu"
              src={require("../assets/images/icon_menu.png")}
            />

            <div
              className="dropdown"
              style={{ display: isMenu ? "block" : "none" }}
            >
              <div className="item-dropdown">
                <Link className="text-dropdown" to="/top">
                  自分の記録
                </Link>
              </div>
              <div className="item-dropdown">
                <Link className="text-dropdown">体重グラフ</Link>
              </div>
              <div className="item-dropdown">
                <Link className="text-dropdown">目標</Link>
              </div>
              <div className="item-dropdown">
                <Link className="text-dropdown">選択中のコース</Link>
              </div>
              <div className="item-dropdown">
                <Link className="text-dropdown">コラム一覧</Link>
              </div>
              <div className="item-dropdown">
                <Link className="text-dropdown">設定</Link>
              </div>

              <div className="item-dropdown" onClick={onPress}>
                <Link className="text-dropdown">
                  {isLogin ? "ログアウト" : "ログイン"}{" "}
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
