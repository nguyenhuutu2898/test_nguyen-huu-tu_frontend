import React, { useState } from "react";

const ScrollTop = () => {
  const [visible, setVisible] = useState(false);

  const toggleVisible = () => {
    const scrolled = document.documentElement.scrollTop;
    if (scrolled > 300) {
      setVisible(true);
    } else if (scrolled <= 300) {
      setVisible(false);
    }
  };

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  window.addEventListener("scroll", toggleVisible);

  return (
    <div onClick={scrollToTop} className="scrollTop">
      <img alt="scroll"
        src={require("../assets/images/component_scroll.png")}
        style={{ display: visible ? "inline" : "none" }}
      />
    </div>
  );
};

export default ScrollTop;
