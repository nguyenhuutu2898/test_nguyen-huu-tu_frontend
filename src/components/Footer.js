import React from "react";

const Footer = () => {
  return (
    <div className="bg-footer">
        <div className="container">
            <div className="d-flex gap-5">
                <p className="text-footer">会員登録</p>
                <p className="text-footer">運営会社</p>
                <p className="text-footer">利用規約</p>
                <p className="text-footer">個人情報の取扱について</p>
                <p className="text-footer">特定商取引法に基づく表記</p>
                <p className="text-footer">お問い合わせ</p>
            </div>
        </div>

    </div>
  );
};

export default Footer;
