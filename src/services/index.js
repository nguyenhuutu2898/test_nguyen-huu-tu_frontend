const column = [
  {
    id: "1",
    src: "column-1.jpg",
    description: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
  },
  {
    id: "2",
    src: "column-2.jpg",
    description: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
  },
  {
    id: "3",
    src: "column-3.jpg",
    description: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
  },
  {
    id: "4",
    src: "column-4.jpg",
    description: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
  },
  {
    id: "5",
    src: "column-5.jpg",
    description: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
  },
  {
    id: "6",
    src: "column-1.jpg",
    description: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
  },
  {
    id: "7",
    src: "column-7.jpg",
    description: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
  },
  {
    id: "8",
    src: "column-8.jpg",
    description: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
  },
  {
    id: "9",
    src: "column-1.jpg",
    description: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
  },
  {
    id: "10",
    src: "column-2.jpg",
    description: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
  },
  {
    id: "11",
    src: "column-7.jpg",
    description: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
  },
  {
    id: "12",
    src: "column-8.jpg",
    description: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
  },
  {
    id: "13",
    src: "column-1.jpg",
    description: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
  },
  {
    id: "14",
    src: "column-2.jpg",
    description: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
  },
  {
    id: "15",
    src: "column-7.jpg",
    description: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
  },
  {
    id: "16",
    src: "column-8.jpg",
    description: "魚を食べて頭もカラダも元気に！知っておきたい魚を食べるメリ…",
  },
];

const topPage = [
  {
    id: "1",
    src: "m01.jpg",
  },
  {
    id: "2",
    src: "m02.jpg",
  },
  {
    id: "3",
    src: "m03.jpg",
  },
  {
    id: "4",
    src: "l01.jpg",
  },
  {
    id: "5",
    src: "m01.jpg",
  },
  {
    id: "6",
    src: "m03.jpg",
  },
  {
    id: "7",
    src: "m02.jpg",
  },
  {
    id: "8",
    src: "l01.jpg",
  },
  {
    id: "9",
    src: "m01.jpg",
  },
  {
    id: "10",
    src: "m03.jpg",
  },
  {
    id: "11",
    src: "m01.jpg",
  },
  {
    id: "12",
    src: "l01.jpg",
  },
  {
    id: "13",
    src: "m01.jpg",
  },
  {
    id: "14",
    src: "m01.jpg",
  },
  {
    id: "15",
    src: "m02.jpg",
  },
  {
    id: "16",
    src: "l01.jpg",
  },
];

export const getColumn = (limit, offset) => {
  return column.slice(offset, offset + limit);
};

export const getTopPage = (limit, offset) => {
  return topPage.slice(offset, offset + limit);
};
